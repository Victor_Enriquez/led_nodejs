var express = require('express');
var router = express.Router();
var Host = require('../models/ips');
var getIP = require('ipware')().get_ip;
var Gpio = require('onoff').Gpio;
var LED2 = new Gpio(27,'out');
var LED1 = new Gpio(17,'out');


router.get('/',function(req,res,next){
	res.render('index',{title:'Equipo 5'});
});

router.get('/led/:id/:id2',function(req,res,next){

	var id = req.params.id;   //---Select Led
	var id2 = req.params.id2; //---On/Off

//---Seleccionar led ( 1 o 2 )	
if(id == '1'){
	//----prender led
	if(id2 == '1'){
	   LED1.writeSync(1);
	}else{
	//----apagar led
	   LED1.writeSync(0);
	}
}else{
	//----prender led
	if(id2 == '1'){
	   LED2.writeSync(1);
	}else{
	//----apagar led
	   LED2.writeSync(0);
	}
};

//---Almacena una cadena para guardarla en la DB
var flag = '';
if(id2 == '0'){
flag = 'Apagado';
}else{
flag='Prendido';
};

//----Obtener ip
var ipInfo = getIP(req);
    var clientIp = ipInfo.clientIp;
    var ip = clientIp.substring(7,21);
    var json= {"host":ip,"led":id,"status":flag};

//----Insertar en base de datos
Host.build(json).save();
res.redirect('/');
//res.json(json);
	
});

module.exports =router;